# Zensations Frontend Workshop

## Tools

### [Homebrew](http://brew.sh) (Paketmanager)

Paketverwaltung für OSX, wird verwendet um andere Tools zu installieren.
Terminal öffnen - `CMD + Leertaste` und "Terminal" zu tippen anfangen - Enter.
Dort folgendes Kommando hineinkopieren:

`ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"`

### Git (Versionskontrolle)

`brew install git`

### Node (Javascript Engine)

`brew install node`

### Brackets (Editor)

[Brackets](http://brackets.io "Brackets") herunterladen und in den Programmordner legen.

## Projektstart

Wir klonen das aktuelle Repository ...

`git clone https://bitbucket.org/zensations/frontend-workshop.git`

... wechseln in das Verzeichnis ...

`cd frontent-workshop`

... wechseln in die Startposition ...

`git checkout lesson-0`

... und öffnen das Verzeichnis mit Brackets.

Los gehts!


